class MyDictionary {
  // Main Driver Method (optional in our modular design)
  public static void main(String args[]) {
    System.out.println("The best way to use this type is to run another app main and instantiate a dictionary in it.");
  }

  private Record[] arr;
  private int size = 0;
  private int count = 0;

  public int Size() {
    return size;
  }

  public int Count() {
    return count;
  }

  MyDictionary(int initialSize) {
    arr = new Record[initialSize];
    size = initialSize;
    count = 0;
  }

  public boolean isEmpty() {
    return count == 0 ? true : false;

  }

  public boolean isFull() {
    return size == count ? true : false;
  }
  private int find(int key)
  { int l = 0;
    int r = count - 1;
    int mid = (l + r) / 2;
    int index = -1;
    while (l <= r) {
      if (arr[mid].getKey() < key) {
        l = mid + 1;
        mid = (l + r) / 2;

      } else if (arr[mid].getKey() > key) {
        r = mid - 1;
        mid = (l + r) / 2;

      } else if (arr[mid].getKey() == key) {

        return mid;

      }

    }
    return index;

    
  }

  public double get(int key) {
    if(find(key)==-1)
    {return -1;}
    else
    {return arr[find(key)].getValue();}
  }

  public void Insert(double elem) {
    if (size > count) {
      Record rec = new Record(elem);
      int l = 0;
      int r = count - 1;
      int mid = (l + r) / 2;
      int index = -1;
      while (l <= r) {
        if (arr[mid].getKey() < rec.getKey()) {
          l = mid + 1;
          mid = (l + r) / 2;

        } else if (arr[mid].getKey() > rec.getKey()) {
          r = mid - 1;
          index = mid;
          mid = (l + r) / 2;

        } else if (arr[mid].getKey() == rec.getKey()) {
          arr[mid] = rec;

          return;

        }

      }
      if (index == -1) {
        arr[count++] = rec;
        return;
      }
      count++;
      for (int i = index + 1; i < count; i++) {
        arr[i] = arr[i - 1];

      }
      arr[index] = rec;

    } else {
      System.out.println("Size" + size + " not enough for holding an extra element after " + count + " count");
    }
  }

  public void Show() {
    System.out.print("Printing Dictionary of " + size + " Size and " + count + " Occupancy:");
    for (int i = 0; i < count; i++) {
      System.out.print(" " + arr[i].getValue());
    }
    for (int i = count; i < size; i++) {
      System.out.print(" .");
    }
    System.out.println("");
  }
}