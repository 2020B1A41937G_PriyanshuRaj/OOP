public class Record {
   private int key;
   private double value;

   Record(double v) {
      value = v;
      key = (int) value;
      if (value - key >= 0.5) {
         key++;
      } else if (value - key <= -0.5) {
         key--;

      }
   }
    public int getKey()
   {return key;

   }
   public double getValue()
   {return value;

   }

}
